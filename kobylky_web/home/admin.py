from django.contrib import admin

from content.models import Content
from .models import Home

class HomeAdmin(admin.ModelAdmin):
    pass

admin.site.register(Home, HomeAdmin)

