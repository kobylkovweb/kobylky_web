from django.db import models
from django.utils.translation import ugettext_lazy as _

from . import managers

class Home(models.Model):
    # Relations
    content = models.OneToOneField(
        'content.Content',
        primary_key=True,
        related_name="home",
        verbose_name=_("content"))
    # Atributes - Mandatory
    pos = models.PositiveIntegerField(
        verbose_name=_("position"))
    # Atributes - Optional
    # Object Manager
    objects = managers.HomeManager()

    # Custom Properties
    # Methods
    # Meta and String
    class Meta:
        verbose_name = _("Home")
        verbose_name_plural = _("Home")
        ordering = ("pos", )

    def __str__(self):
        return self.content.title
