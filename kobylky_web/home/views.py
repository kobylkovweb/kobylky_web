# -*- coding: utf-8 -*-
from django.shortcuts import render

from django.views import generic

from .models import Home

# class IndexView(generic.TemplateView):
class IndexView(generic.ListView):
    template_name = "home/index.html"
    context_object_name = 'front_page_article_list'
    model = Home

    def get_queryset(self):
        """Returns the index page entries orderd by their pos."""
        return Home.objects.all().order_by('pos')
