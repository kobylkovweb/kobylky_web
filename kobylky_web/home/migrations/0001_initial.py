# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Home',
            fields=[
                ('content', models.OneToOneField(related_name='home', primary_key=True, serialize=False, to='content.Content', verbose_name='content')),
                ('pos', models.PositiveIntegerField(verbose_name='position')),
            ],
            options={
                'ordering': ('pos',),
                'verbose_name': 'Home',
                'verbose_name_plural': 'Home',
            },
        ),
    ]
