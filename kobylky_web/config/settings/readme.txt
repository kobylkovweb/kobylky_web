Manual - how to choose between different setting files.

Different settings are chosen according to the DJANGO_SETTINGS_MODULE variable.
If this variable is set at the operating system level, it overrides the
the definition in manage.py and wsgi.py. So we set it up during virtualenv
loading:

1) First switch to the right virtualenv:
    workon kobylky_web
2) Navigate to the virtualenvs bin directory:
    cd $VIRTUAL_ENV/bin
3) Add the following line to the postactivate file:
    export DJANGO_SETTINGS_MODULE="config.settings.local"
4) Add the following line to the predeactivate file:
    unset DJANGO_SETTINGS_MODULE

!!! Somehow it does not work now, probably some bug in actual version of
virtualenvwrapper so I have to set this enviroment variable explicitly from
shell each time the virtualenv is activated. !!!
