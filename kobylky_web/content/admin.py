from django.contrib import admin

from .models import Section, Event, Content, Article

admin.site.register(Section)
admin.site.register(Event)
admin.site.register(Content)
admin.site.register(Article)
