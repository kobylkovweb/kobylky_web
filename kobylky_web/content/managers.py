# -*- coding: utf-8 -*-
from django.db import models


class SectionManager(models.Manager):
    pass

class EventManager(models.Manager):
    pass

class ContentManager(models.Manager):
    pass

class ArticleManager(models.Manager):
    pass
