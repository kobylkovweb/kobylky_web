# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Content',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='Enter the title.', max_length=127, verbose_name='title')),
                ('created', models.DateTimeField(verbose_name='created')),
                ('publish', models.BooleanField(verbose_name='publish')),
                ('author', models.ForeignKey(related_name='content', verbose_name='author', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-created',),
                'verbose_name': 'Content',
                'verbose_name_plural': 'Content',
            },
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='Enter the section name.', max_length=127, verbose_name='title')),
                ('pos', models.PositiveIntegerField(verbose_name='position')),
                ('created', models.DateTimeField(verbose_name='created')),
                ('publish', models.BooleanField(verbose_name='publish')),
                ('description', models.TextField(verbose_name='description')),
                ('author', models.ForeignKey(related_name='sections', verbose_name='author', to=settings.AUTH_USER_MODEL)),
                ('parent_section', models.ForeignKey(related_name='sections', verbose_name='parent section', blank=True, to='content.Section', null=True)),
            ],
            options={
                'ordering': ('pos',),
                'verbose_name': 'Section',
                'verbose_name_plural': 'Sections',
            },
        ),
        migrations.AddField(
            model_name='content',
            name='section',
            field=models.ForeignKey(related_name='content', verbose_name='section', to='content.Section'),
        ),
    ]
