# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0002_auto_20150929_1155'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('content', models.OneToOneField(related_name='article', primary_key=True, serialize=False, to='content.Content', verbose_name='content')),
                ('slug', models.SlugField(unique=True, verbose_name='slug')),
                ('pub_date', models.DateTimeField(verbose_name='date published')),
                ('body', models.TextField(verbose_name='body')),
            ],
            options={
                'ordering': ('-pub_date',),
                'verbose_name': 'Article',
                'verbose_name_plural': 'Articles',
            },
        ),
    ]
