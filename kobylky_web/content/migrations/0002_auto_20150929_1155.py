# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='Enter the title.', max_length=127, verbose_name='title')),
                ('created', models.DateTimeField(verbose_name='created')),
            ],
            options={
                'ordering': ('created',),
                'verbose_name': 'Event',
                'verbose_name_plural': 'Events',
            },
        ),
        migrations.AddField(
            model_name='content',
            name='event',
            field=models.ForeignKey(related_name='content', on_delete=django.db.models.deletion.SET_NULL, verbose_name='event', blank=True, to='content.Event', null=True),
        ),
    ]
