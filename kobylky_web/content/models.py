from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from . import managers

class Section(models.Model):
    # Relations
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="sections",
        verbose_name=_("author"))
    parent_section = models.ForeignKey(
        "self",
        related_name="sections",
        verbose_name=_("parent section"),
        blank=True,
        null=True,
        on_delete=models.CASCADE)
    # Attributes - Mandatory
    title = models.CharField(max_length=127,
        verbose_name=_("title"),
        help_text=_("Enter the section name."))
    pos = models.PositiveIntegerField(
        verbose_name=_("position"))
    created = models.DateTimeField(
        verbose_name=_("created"))
    publish = models.BooleanField(
        verbose_name=_("publish"))
    description = models.TextField(
        verbose_name=_("description"),
        blank=True
        )
    
    # Attributes - Optional
    # Object Manager
    objects = managers.SectionManager()

    # Custom Properties
    # Methods
    # Meta and String
    class Meta:
        verbose_name = _("Section")
        verbose_name_plural = _("Sections")
        ordering = ("pos", )
    
    def __str__(self):
        return self.title

class Event(models.Model):
    # Relations
    # Attributes - Mandatory
    title = models.CharField(max_length=127,
        verbose_name=_("title"),
        help_text=_("Enter the title."))
    created = models.DateTimeField(
        verbose_name=_("created"))
    # Attributes - Optional
    # Object Manager
    objects = managers.EventManager()

    # Custom Properties
    # Methods
    # Meta and String
    class Meta:
        verbose_name = _("Event")
        verbose_name_plural = _("Events")
        ordering = ("created", )
    
    def __str__(self):
        return self.title



class Content(models.Model):
    # Relations
    section = models.ForeignKey(
        "Section",
        related_name="content",
        verbose_name=_("section"))
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="content",
        verbose_name=_("author"))
    event = models.ForeignKey(
        "Event",
        related_name="content",
        verbose_name=_("event"),
        blank=True,
        null=True,
        on_delete=models.SET_NULL)
    # Attributes - Mandatory
    title = models.CharField(max_length=127,
        verbose_name=_("title"),
        help_text=_("Enter the title."))
    created = models.DateTimeField(
        verbose_name=_("created"))
    publish = models.BooleanField(
        verbose_name=_("publish"))
    # Attributes - Optional
    # Object Manager
    objects = managers.ContentManager()

    # Custom Properties
    # Methods
    def is_invitation(self):
        return hasattr(self, 'invitation')
    def is_chronicle(self):
        return hasattr(self, 'chronicle')
    # Meta and String
    class Meta:
        verbose_name = _("Content")
        verbose_name_plural = _("Content")
        ordering = ("-created", )
    
    def __str__(self):
        return self.title

class Article(models.Model):
    # Relations
    content = models.OneToOneField(
        "Content",
        primary_key=True,
        related_name="article",
        verbose_name=_("content"))
    # Attributes - Mandatory
    slug = models.SlugField(
        unique=True,
        verbose_name=_("slug"))
    pub_date = models.DateTimeField(
        verbose_name=_("date published"))
    body = models.TextField(
        verbose_name=_("body")
        )
# Attributes - Optional
    # Object Manager
    objects = managers.ArticleManager()

    # Custom Properties
    # Methods
    # Meta and String
    class Meta:
        verbose_name = _("Article")
        verbose_name_plural = _("Articles")
        ordering = ("-pub_date", )
    
    def __str__(self):
        return self.content.title

