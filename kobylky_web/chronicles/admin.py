from django.contrib import admin

from content.models import Content
from .models import Chronicle

class ChronicleAdmin(admin.ModelAdmin):
    pass

admin.site.register(Chronicle, ChronicleAdmin)

