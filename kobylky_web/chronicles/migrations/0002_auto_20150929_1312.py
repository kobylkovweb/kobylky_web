# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('chronicles', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='chronicle',
            options={'ordering': ('begin',), 'verbose_name': 'Chronicle', 'verbose_name_plural': 'Chronicles'},
        ),
    ]
