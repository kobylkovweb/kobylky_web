# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Chronicle',
            fields=[
                ('content', models.OneToOneField(related_name='chronicle', primary_key=True, serialize=False, to='content.Content', verbose_name='content')),
                ('slug', models.SlugField(unique=True, verbose_name='slug')),
                ('begin', models.DateTimeField(verbose_name='begin')),
                ('end', models.DateTimeField(verbose_name='end')),
                ('body', models.TextField(verbose_name='body')),
            ],
            options={
                'ordering': ('-begin',),
                'verbose_name': 'Chronicle',
                'verbose_name_plural': 'Chronicle',
            },
        ),
    ]
