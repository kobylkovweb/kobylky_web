# -*- config: utf-8 -*-
from selenium import webdriver
from django.core.urlresolvers import reverse
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import unittest
import os

os.environ['DJANGO_LIVE_TEST_SERVER_ADDRESS'] = 'kobylky:8082'

class NewVisitorTest(unittest.TestCase):
# class NewVisitorTest(StaticLiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    # def get_full_url(self, namespace):
    #     return self.live_server_url + reverse(namespace)

    def test_home_title(self):
        # self.browser.get(self.get_full_url("home"))
        # print("url = " + self.get_full_url("home"))
        # print("url = " + '%s%s' % (self.live_server_url, "/"))
        # self.browser.get(self.live_server_url)
        self.browser.get("http://kobylky")
        self.assertIn('Vítejte v systému Django', self.browser.title)

    def test_summary_css(self):
        self.browser.get("http://kobylky")
        summary = self.browser.find_element_by_id("summary")
        # print("css = " + str(summary.value_of_css_property(
        #     "background-color")))
        self.assertEqual(summary.value_of_css_property("background-color"), 
                         "rgba(224, 235, 255, 1)")

if __name__ == '__main__':
    unittest.main(warnings='ignore')
