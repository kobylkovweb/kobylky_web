from django.db import models

from django.utils.translation import ugettext_lazy as _

from . import managers

class News(models.Model):
    # Relations
    content = models.OneToOneField(
        "content.Content",
        primary_key=True,
        related_name="news",
        verbose_name=_("content"))
    # Attributes - Mandatory
    body = models.TextField(
        verbose_name=_("body")
        )
# Attributes - Optional
    # Object Manager
    objects = managers.NewsManager()

    # Custom Properties
    # Methods
    # Meta and String
    class Meta:
        verbose_name = _("News")
        verbose_name_plural = _("News")
    
    def __str__(self):
        return str(self.content.created)

