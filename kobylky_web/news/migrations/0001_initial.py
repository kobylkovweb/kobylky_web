# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0004_auto_20150929_1311'),
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('content', models.OneToOneField(related_name='news', primary_key=True, serialize=False, to='content.Content', verbose_name='content')),
                ('body', models.TextField(verbose_name='body')),
            ],
            options={
                'verbose_name': 'News',
                'verbose_name_plural': 'News',
            },
        ),
    ]
