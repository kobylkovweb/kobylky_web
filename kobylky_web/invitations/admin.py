from django.contrib import admin

from content.models import Content
from .models import Invitation

class InvitationAdmin(admin.ModelAdmin):
    pass

admin.site.register(Invitation, InvitationAdmin)

