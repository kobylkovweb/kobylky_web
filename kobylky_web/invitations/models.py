from django.db import models
from django.utils.translation import ugettext_lazy as _

from . import managers

class Invitation(models.Model):
    # Relations
    content = models.OneToOneField(
        'content.Content',
        primary_key=True,
        related_name="invitation",
        verbose_name=_("content"))
    # Atributes - Mandatory
    slug = models.SlugField(
        unique=True,
        verbose_name=_("slug"))
    begin = models.DateTimeField(
        verbose_name=_("begin"))
    end = models.DateTimeField(
        verbose_name=_("end"))
    body = models.TextField(
        verbose_name=_("body")
        )
    # Atributes - Optional
    # Object Manager
    objects = managers.InvitationManager()

    # Custom Properties
    # Methods
    # Meta and String
    class Meta:
        verbose_name = _("Invitation")
        verbose_name_plural = _("Invitations")
        ordering = ("-begin", )

    def __str__(self):
        return self.content.title
